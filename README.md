# Wallapop-test

This test app is a test for the appliance to Wallpop.

Author: Pau Aza Puig

## Development server

Run `npm install` to setup the project dependencies.

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
