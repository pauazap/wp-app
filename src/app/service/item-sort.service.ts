import { Injectable } from '@angular/core';

// Pipes
import { FiterPipe } from '../pipes/filter.pipe';

// Interface
import { Item } from 'src/app/interface/item-interface';
import { FilterOption } from 'src/app/interface/filter-option-interface';

// Constants
import {
  TITLE_ASC, TITLE_DESC, DESCRIPTION_ASC, DESCRIPTION_DESC,
  PRICE_ASC, PRICE_DESC, EMAIL_ASC, EMAIL_DESC, FILTER_OPTIONS
} from 'src/app/constants/search-constants';
import { SEARCH_FILTER_FIELDS } from 'src/app/pipes/filter-constants';

@Injectable()
export class ItemActionsService {

  constructor(
    private _filterPipe: FiterPipe
  ) {}

  filterItems(items: Item[], textFilter: string, sortingCriteria: number): Item[] {
    let filteredItems = [ ...items ];
    filteredItems = this._applyFilter(filteredItems, textFilter);
    filteredItems = this._sort(filteredItems, sortingCriteria);
    return filteredItems;
  }

  private _applyFilter(items: Item[], textFilter: string): Item[] {
    return this._filterPipe.transform(items, textFilter, SEARCH_FILTER_FIELDS);
  }

  private _sort(items: Item[], sortingCriteria: number): Item[] {
    const sortOption: FilterOption = this._getOptionFromValue(sortingCriteria);
    switch (sortingCriteria) {
      case TITLE_ASC.id:
      case DESCRIPTION_ASC.id:
      case EMAIL_ASC.id:
        items = items.sort(
          (itemA: Item, itemB: Item) => itemA[sortOption.field].toLowerCase() > itemB[sortOption.field].toLowerCase() ? 1 : -1
        );
        break;
      case TITLE_DESC.id:
      case DESCRIPTION_DESC.id:
      case EMAIL_DESC.id:
        items = items.sort(
          (itemA: Item, itemB: Item) => itemA[sortOption.field].toLowerCase() < itemB[sortOption.field].toLowerCase() ? 1 : -1
        );
        break;
        case PRICE_ASC.id:
          items = items.sort(
            (itemA: Item, itemB: Item) => parseInt(itemA[sortOption.field], 10) > parseInt(itemB[sortOption.field], 10) ? 1 : -1
          );
          break;
        case PRICE_DESC.id:
          items = items.sort(
            (itemA: Item, itemB: Item) => parseInt(itemA[sortOption.field], 10) < parseInt(itemB[sortOption.field], 10) ? 1 : -1
          );
          break;
    }
    return items;
  }

  private _getOptionFromValue(value: number): FilterOption {
    return FILTER_OPTIONS.find( (option: FilterOption) => option.id === value );
  }

}
