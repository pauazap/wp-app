import { Injectable } from '@angular/core';

// Interface
import { Item } from 'src/app/interface/item-interface';

@Injectable()
export class ItemFavService {

  private _favourites: Item[] = [];

  getFavouriteItems(): Item[] {
    return this._favourites;
  }

  addFavourite(item: Item): void {
    this._favourites.push(item);
  }

  removeFavourite(fav: Item): void {
    this._favourites = this._favourites.filter(
      (item: Item) => item.id !== fav.id
    );
  }

}
