import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// Interface
import { Item } from 'src/app/interface/item-interface';

@Injectable()
export class ItemApiService {

  API_URL = '/items.json';

  constructor(
    private http: HttpClient
  ) { }

  getItemsData(): Observable<Item[]> {
    return this.http.get<Item[]>(`${this.API_URL}`).pipe(
      map(
        (response: any) => {
          const items: Item[] = response.items;
          items.forEach( (item: Item, index: number) => item.id = index );
          return items;
        }
      )
    );
  }

}
