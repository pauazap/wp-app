import { NgModule } from '@angular/core';

import { FiterPipe } from './filter.pipe';

@NgModule({
  declarations: [
    FiterPipe
  ],
  exports: [
    FiterPipe
  ]
})
export class PipesModule {}
