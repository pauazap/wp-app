import { Pipe, PipeTransform } from '@angular/core';

// Interfaces
import { Item } from 'src/app/interface/item-interface';

@Pipe({
  name: 'filterFields'
})
export class FiterPipe implements PipeTransform {

  transform(items: Item[], textSearch: string, searchFields: string[]): Item[] {
    return (items || []).filter(
      (item: Item) => searchFields.some( (field: string) => new RegExp(textSearch.toLowerCase(), 'gi').test(item[field].toLowerCase()) )
    );
  }

}
