const FILTER_TITLE = 'title';
const FILTER_DESCRIPTION = 'description';
const FILTER_PRICE = 'price';
const FILTER_EMAIL = 'email';

export const SEARCH_FILTER_FIELDS: string[] = [
  FILTER_TITLE,
  FILTER_DESCRIPTION,
  FILTER_PRICE,
  FILTER_EMAIL
];

export const FAV_FILTER_FIELDS: string[] = [ FILTER_TITLE ];
