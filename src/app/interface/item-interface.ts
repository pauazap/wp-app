export interface Item {
  id: number;
  title: string;
  description: string;
  email: string;
  image: string;
  price: string;
  favourite: boolean;
}
