export interface FilterOption {
  id: number;
  value: string;
  field?: string;
}
