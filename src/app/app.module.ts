import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

// Modules
import { WpFavModalModule } from './components/wp-fav-modal/wp-fav-modal.module';
import { WpItemElementModule } from './components/wp-item-element/wp-item-element.module';
import { WpItemListModule } from './components/wp-item-list/wp-item-list.module';
import { WpItemSearchModule } from './components/wp-item-search/wp-item-search.module';

// Pipes
import { FiterPipe } from './pipes/filter.pipe';

// Components
import { AppComponent } from './app.component';

// Services
import { ItemApiService } from './service/item-api.service';
import { ItemFavService } from './service/item-fav.service';
import { ItemActionsService } from './service/item-sort.service';

const MODULES = [
  WpFavModalModule,
  WpItemElementModule,
  WpItemListModule,
  WpItemSearchModule
];

const COMPONENTS = [
  AppComponent
];

@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ...MODULES
  ],
  providers: [
    ItemApiService,
    ItemFavService,
    ItemActionsService,
    FiterPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
