import { FilterOption } from 'src/app/interface/filter-option-interface';

// Default items per page
export const ITEMS_PER_PAGE = 5;

// Search filters
export const BASE_OPTION: FilterOption = { id: -1, value: 'Select order' };
export const TITLE_ASC: FilterOption = { id: 0, value: 'Title A-Z', field: 'title' };
export const TITLE_DESC: FilterOption = { id: 1, value: 'Title Z-A', field: 'title' };
export const DESCRIPTION_ASC: FilterOption = { id: 2, value: 'Description A-Z', field: 'description' };
export const DESCRIPTION_DESC: FilterOption = { id: 3, value: 'Description Z-A', field: 'description' };
export const PRICE_ASC: FilterOption = { id: 4, value: 'Price cheap-expensive', field: 'price' };
export const PRICE_DESC: FilterOption = { id: 5, value: 'Price expensive-cheap', field: 'price' };
export const EMAIL_ASC: FilterOption = { id: 6, value: 'Email A-Z', field: 'email' };
export const EMAIL_DESC: FilterOption = { id: 7, value: 'Email Z-A', field: 'email' };

export const FILTER_OPTIONS: FilterOption[] = [
  BASE_OPTION,
  TITLE_ASC,
  TITLE_DESC,
  DESCRIPTION_ASC,
  DESCRIPTION_DESC,
  PRICE_ASC,
  PRICE_DESC,
  EMAIL_ASC,
  EMAIL_DESC
];
