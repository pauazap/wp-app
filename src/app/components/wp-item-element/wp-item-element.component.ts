import { Component, Input, Output, EventEmitter, HostBinding, OnInit } from '@angular/core';

// Services
import { ItemFavService } from 'src/app/service/item-fav.service';

// Interface
import { Item } from 'src/app/interface/item-interface';

// Constants
import { MODE_LIST, MODE_FAV } from 'src/app/constants/list-constants';

@Component({
  selector: 'wp-item-element',
  templateUrl: './wp-item-element.component.html',
  styleUrls: ['./wp-item-element.component.scss']
})
export class WpItemElementComponent implements OnInit {

  @Input() item: Item;
  @Input() mode: string = MODE_LIST;

  @Output() toggleFav: EventEmitter<any> = new EventEmitter<any>();

  @HostBinding('class.c-item__favourite') favouriteItem = false;

  constructor(
    private _itemFavService: ItemFavService
  ) {}

  ngOnInit(): void {
    this.favouriteItem = this.mode === MODE_FAV;
  }

  toggleFavourite(): void {
    this.item.favourite = !this.item.favourite;
    if (this.item.favourite) {
      this._itemFavService.addFavourite(this.item);
    } else {
      this._itemFavService.removeFavourite(this.item);
    }
    this.toggleFav.emit();
  }

}
