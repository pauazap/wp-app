import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { WpItemElementComponent } from './wp-item-element.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    WpItemElementComponent
  ],
  exports: [
    WpItemElementComponent
  ]
})
export class WpItemElementModule {}
