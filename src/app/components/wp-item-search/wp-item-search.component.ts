import { Component, OnInit } from '@angular/core';

// Services
import { ItemApiService } from 'src/app/service/item-api.service';
import { ItemActionsService } from 'src/app/service/item-sort.service';

// Interface
import { Item } from 'src/app/interface/item-interface';
import { FilterOption } from 'src/app/interface/filter-option-interface';

// Constants
import { ITEMS_PER_PAGE, FILTER_OPTIONS, BASE_OPTION } from 'src/app/constants/search-constants';

@Component({
  selector: 'wp-item-search',
  templateUrl: './wp-item-search.component.html',
  styleUrls: ['./wp-item-search.component.scss']
})
export class WpItemSearchComponent implements OnInit {

  allItems: Item[] = [];
  currentFilterItems: Item[] = [];
  items: Item[] = [];
  favOpen = false;
  currentOffset = ITEMS_PER_PAGE;

  // Item search
  fieldValue = '';

  // Item sort
  filterMode: any = BASE_OPTION.id;
  filterOptions: FilterOption[] = FILTER_OPTIONS;

  constructor(
    private _itemApiService: ItemApiService,
    private _itemActionsService: ItemActionsService
  ) {}

  ngOnInit(): void {
    this._getAllItemsData();
  }

  /**
   * Open favourites popup
   */
  showFavourites(): void {
    this.favOpen = true;
  }

  /**
   * Apply filter and sorting to item list
   */
  filterItems(): void {
    this.currentOffset = ITEMS_PER_PAGE;
    this.currentFilterItems = this._itemActionsService.filterItems(this.allItems, this.fieldValue, parseInt(this.filterMode, 10));
    this._applyFilters();
  }

  /**
   * Load all items from data source
   */
  private _getAllItemsData(): void {
    this._itemApiService.getItemsData().subscribe(
      (items: any) => {
        this.allItems = items;
        this.currentFilterItems = [ ...this.allItems ];
        this._applyFilters();
      }
    );
  }

  /**
   * Load more items
   */
  loadMore(): void {
    this.currentOffset += ITEMS_PER_PAGE;
    this._applyFilters();
  }

  /**
   * Apply current offset to items result
   */
  private _applyFilters(): void {
    this.items = [ ...this.currentFilterItems.slice(0, this.currentOffset) ];
  }

}
