import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Modules
import { PipesModule } from 'src/app/pipes/pipes.module';
import { WpFavModalModule } from '../wp-fav-modal/wp-fav-modal.module';
import { WpItemListModule } from 'src/app/components/wp-item-list/wp-item-list.module';

// Components
import { WpItemSearchComponent } from './wp-item-search.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipesModule,
    WpFavModalModule,
    WpItemListModule
  ],
  declarations: [
    WpItemSearchComponent
  ],
  exports: [
    WpItemSearchComponent
  ]
})
export class WpItemSearchModule {}
