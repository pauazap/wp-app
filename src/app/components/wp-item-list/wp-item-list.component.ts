import { Component, Input, Output, EventEmitter } from '@angular/core';

// Interface
import { Item } from 'src/app/interface/item-interface';

// Constants
import { MODE_LIST, MODE_FAV } from 'src/app/constants/list-constants';

@Component({
  selector: 'wp-item-list',
  templateUrl: './wp-item-list.component.html',
  styleUrls: ['./wp-item-list.component.scss']
})
export class WpItemListComponent {

  @Input() items: Item[] = [];
  @Input() mode: string = MODE_LIST;

  @Output() removeFav: EventEmitter<Item> = new EventEmitter<Item>();

  readonly MODE_FAV = MODE_FAV;

  toggleFav(item: Item): void {
    if (!item.favourite) {
      this.removeFav.emit(item);
    }
  }

}
