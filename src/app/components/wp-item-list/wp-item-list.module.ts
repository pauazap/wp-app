import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { WpItemElementModule } from 'src/app/components/wp-item-element/wp-item-element.module';

// Components
import { WpItemListComponent } from './wp-item-list.component';

@NgModule({
  imports: [
    CommonModule,
    WpItemElementModule
  ],
  declarations: [
    WpItemListComponent
  ],
  exports: [
    WpItemListComponent
  ]
})
export class WpItemListModule {}
