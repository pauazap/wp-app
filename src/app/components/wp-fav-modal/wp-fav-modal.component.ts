import { Component, OnInit, Output, EventEmitter } from '@angular/core';

// Services
import { ItemFavService } from 'src/app/service/item-fav.service';

// Interface
import { Item } from 'src/app/interface/item-interface';

// Constants
import { MODE_FAV } from 'src/app/constants/list-constants';
import { FAV_FILTER_FIELDS } from 'src/app/pipes/filter-constants';

@Component({
  selector: 'wp-fav-modal',
  templateUrl: './wp-fav-modal.component.html',
  styleUrls: ['./wp-fav-modal.component.scss']
})
export class WpFavModalComponent implements OnInit {

  @Output() closeModal: EventEmitter<boolean> = new EventEmitter<boolean>();

  items: Item[] = [];

  readonly MODE_FAV = MODE_FAV;

  // Input search
  fieldValue = '';
  readonly FILTERS = FAV_FILTER_FIELDS;

  constructor(
    private _itemFavService: ItemFavService
  ) {}

  ngOnInit(): void {
    this._getFavItems();
  }

  remove(item: Item): void {
    this.items = this.items.filter( (fav: Item) => fav.id !== item.id );
  }

  private _getFavItems(): void {
    this.items = this._itemFavService.getFavouriteItems();
  }

}
