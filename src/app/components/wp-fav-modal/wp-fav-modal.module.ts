import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Modules
import { PipesModule } from 'src/app/pipes/pipes.module';
import { WpItemListModule } from 'src/app/components/wp-item-list/wp-item-list.module';

// Components
import { WpFavModalComponent } from './wp-fav-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipesModule,
    WpItemListModule
  ],
  declarations: [
    WpFavModalComponent
  ],
  exports: [
    WpFavModalComponent
  ]
})
export class WpFavModalModule {}
